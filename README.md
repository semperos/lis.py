# Reimplementation of Peter Norvig's lis.py

The commits in this repository are a step-by-step "reimplementation" of Peter Norvig's [lis.py](http://norvig.com/lispy.html). The steps are intended to be baby steps and are targeted for presenting this implementation by stepping through the commit log.

## License

All of the original Python code is (c) Peter Norvig, 2010-14; See http://norvig.com/lispy.html

Any additional Python code (esp. tests) is (c) Daniel Gregoire, 2016, licensed under the Apache License (Version 2.0).
