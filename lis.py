# -*- coding: utf-8 -*-

from __future__ import division
import sys

# __Types__ #

# A Lisp Symbol is implemented as a Python str
Symbol = str
# A Lisp List is implemented as a Python list
List = list
# A Lisp Number is implemented as a Python int or float
Number = (int, float)

# __Reading__ #


def parse(program):
    """Read a Scheme expression from a string."""
    return read_from_tokens(tokenize(program))

# Lisp usually calls parse `read`
read = parse


def tokenize(program):
    """Convert a string into a list of tokens."""
    return program.replace('(', ' ( ').replace(')', ' ) ').split()


def read_from_tokens(tokens):
    """Read an expression from a sequence of tokens."""
    if len(tokens) == 0:
        raise SyntaxError('unexpected EOF while reading')
    token = tokens.pop(0)
    if '(' == token:
        L = List([])
        while tokens[0] != ')':
            L.append(read_from_tokens(tokens))
        tokens.pop(0)  # pop off ')'
        return L
    elif ')' == token:
        raise SyntaxError('unexpected )')
    else:
        return atom(token)


def atom(token):
    """Numbers become numbers; every other token is a symbol."""
    try:
        return int(token)
    except ValueError:
        try:
            return float(token)
        except ValueError:
            return Symbol(token)

# __Environments__ #


def print_it(x):
    """Print x without any additional newline"""
    sys.stdout.write(lisp_print_str(x))


def println(x):
    """Print x and append a newline"""
    print x


def newline():
    """Print a newline"""
    sys.stdout.write("\n")


def standard_env():
    """An environment with some Scheme standard procedures."""
    import math
    import operator as op
    env = Env()
    env.update(vars(math))
    env.update({'+': op.add,
                '-': op.sub,
                '*': op.mul,
                '/': op.div,
                '>': op.gt,
                '<': op.lt,
                '>=': op.ge,
                '<=': op.le,
                '=': op.eq,
                'eq?': op.is_,
                'equal?': op.eq,
                '<<': op.lshift,
                '>>': op.rshift,
                'xor': op.xor,
                'abs': abs,
                'max': max,
                'min': min,
                'round': round,
                'number?': lambda x: isinstance(x, Number),
                # Lists
                'append': op.add,
                'car': lambda x: x[0],
                'cdr': lambda x: x[1:],
                'cons': lambda x, y: [x] + y,
                'length': len,
                'list': lambda *x: List(x),
                'list?': lambda x: isinstance(x, List),
                'null?': lambda x: x == [],
                # Symbols
                'symbol?': lambda x: isinstance(x, Symbol),
                # Logical conditions
                'and': lambda x, y: x and y,
                'not': op.not_,
                'or': lambda x, y: x or y,
                # Effects
                'display': print_it,
                'exit': sys.exit,
                'load-file': lambda x: load_file(x),
                'newline': newline,
                'print': print_it,
                'println': println,
                'begin': lambda *x: x[-1]})
    return env


class Env(dict):
    def __init__(self, inner=(), outer=None):
        """An environment: a dict of {'var':val} pairs, with an outer Env."""
        self.update(inner)
        self.outer = outer

    def find(self, var):
        """Find the innermost Env where var appears."""
        try:
            return self if (var in self) else self.outer.find(var)
        except AttributeError:
            raise LookupError("Undefined symbol '%s'" % var)

# __Lisp Functions__ #


class Procedure(object):
    "A user-defined Scheme procedure."

    def __init__(self, params, body, env):
        self.params, self.body, self.env = params, body, env

    def __call__(self, *args):
        # The function-local environment is a mapping of the
        # function's parameters to their values when the
        # function is being called (which is here).
        env = zip(self.params, args)
        # If we evaluate the body of the function (the
        # parsed but not-yet-evaluated forms) in the context
        # of this function-local environment, the "free"
        # variables that represent the function's parameters
        # will have values now. Any other symbols will be found
        # in the outer scope (i.e., the outer environment(s))
        # via the Env.find method.
        return eval(self.body, Env(inner=env, outer=self.env))

# __Interaction: A REPL__ #


def repl(prompt='lis.py> '):
    """A prompt-read-eval-print loop."""
    while True:
        try:
            val = eval(read(raw_input(prompt)))
            if val is not None:
                print(lisp_print_str(val))
        except Exception as e:
            print(e)


def lisp_print_str(exp):
    "Convert a Python object back into a Lisp-readable string."
    if isinstance(exp, List):
        return '(' + ' '.join(map(lisp_print_str, exp)) + ')'
    else:
        return str(exp)

# __Evaluation__ #

global_env = standard_env()


def eval(x, env=global_env):
    """Evaluate an expression in an environment."""
    if env is None:
        env = standard_env()

    if isinstance(x, Symbol):
        return env.find(x)[x]
    elif not isinstance(x, List):  # constant literal
        return x
    elif x[0] == 'quote':  # (quote exp)
        (_, exp) = x
        return exp
    elif x[0] == 'if':  # (if test conseq alt)
        (_, test, conseq, alt) = x
        exp = (conseq if eval(test, env) else alt)
        return eval(exp, env)
    elif x[0] == 'define':  # (define var exp)
        (_, var, exp) = x
        env[var] = eval(exp, env)
    elif x[0] == 'set!':  # (set! var exp)
        (_, var, exp) = x
        env.find(var)[var] = eval(exp, env)
    elif x[0] == 'lambda':  # (lambda (var...) body)
        (_, params, body) = x
        return Procedure(params, body, env)
    else:
        proc = eval(x[0], env)
        args = [eval(exp, env) for exp in x[1:]]
        return proc(*args)


def load_file(file_name):
    code = ''
    with open(file_name, 'r') as code_file:
        code += code_file.read()
    return eval(read(str('(begin ' + code + ')')))


if __name__ == "__main__":
    try:
        repl()
    except KeyboardInterrupt:
        print "\nExiting..."
