# -*- coding: utf-8 -*-
import unittest
import math

from lis import (atom, Env, eval, load_file, lisp_print_str, List, parse,
                 Procedure, read_from_tokens, read, standard_env, tokenize,
                 Symbol)


class ParserTest(unittest.TestCase):
    def test_parse(self):
        self.assertEqual(parse("42"), 42)
        self.assertEqual(parse("42.3"), 42.3)
        self.assertEqual(parse("(+ 2 3)"), List(["+", 2, 3]))

    def test_tokenize(self):
        self.assertEqual(tokenize("42"), ["42"])

    def test_read_from_tokens(self):
        with self.assertRaises(SyntaxError):
            read_from_tokens([])
        self.assertEqual(read_from_tokens(["42"]), 42)
        self.assertEqual(read_from_tokens(["42.3"]), 42.3)
        self.assertEqual(
            read_from_tokens(["(", "+", "2", "3", ")"]), List(["+", 2, 3]))

    def test_atom(self):
        self.assertEqual(atom("42"), 42)
        self.assertEqual(atom("42.3"), 42.3)
        self.assertTrue(isinstance(atom("foo"), Symbol))


class EnvTest(unittest.TestCase):
    def test_env_ctor(self):
        self.assertIsInstance(Env(), Env)

    def test_env_ctor_starting_value(self):
        env = Env({'pi': 3.14159})
        self.assertEqual(env['pi'], 3.14159)

    def test_env_ctor_outer_env(self):
        env_outer = Env({'e': 2.71828})
        env = Env(inner={'pi': 3.14159}, outer=env_outer)
        self.assertEqual(env['pi'], 3.14159)
        self.assertEqual(env.outer['e'], 2.71828)

    def test_find(self):
        env_outer_outer = Env({'golden_ratio': 1.61803})
        env_outer = Env(inner={'e': 2.71828}, outer=env_outer_outer)
        env = Env(inner={'pi': 3.14159}, outer=env_outer)
        self.assertEqual(env.find('pi')['pi'], 3.14159)
        self.assertEqual(env.find('e')['e'], 2.71828)
        self.assertEqual(env.find('golden_ratio')['golden_ratio'], 1.61803)

    def test_standard_env(self):
        self.assertIsInstance(standard_env(), dict)
        self.assertEqual(standard_env()['pi'], math.pi)
        self.assertEqual(standard_env()['e'], math.e)


class EvaluatorTest(unittest.TestCase):
    def test_eval(self):
        self.assertEqual(eval(42, None), 42)
        self.assertEqual(eval("pi", None), math.pi)
        self.assertEqual(eval("e", standard_env()), math.e)

    def test_eval_scoped(self):
        env = Env(inner=(), outer=Env({'pi': 3.14159}))
        self.assertEqual(eval("pi", env), 3.14159)

    def test_quote(self):
        self.assertEqual(eval(List(["quote", List([2, 3])])), List([2, 3]))

    def test_define(self):
        env = standard_env()
        self.assertIsNone(eval(["define", "golden_ratio", 1.61803], env))
        self.assertEqual(eval("golden_ratio", env), 1.61803)

    def test_set_bang(self):
        env = standard_env()
        self.assertIsNone(eval(["define", "golden_ratio", 1.61803], env))
        self.assertEqual(eval("golden_ratio", env), 1.61803)
        # Adding one more digit to φ (golden ratio)
        self.assertIsNone(eval(["set!", "golden_ratio", 1.618034], env))
        self.assertEqual(eval("golden_ratio", env), 1.618034)

    def test_set_bang_scoped(self):
        env = Env(inner=(), outer=Env({'pi': 3.14159}))
        self.assertEqual(eval("pi", env), 3.14159)
        self.assertIsNone(eval(["set!", "pi", 3.14], env))
        self.assertEqual(eval("pi", env), 3.14)
        self.assertEqual(env.outer['pi'], 3.14)

    def test_if(self):
        self.assertEqual(eval(["if", "pi", "pi", "e"]), math.pi)
        self.assertEqual(eval(["if", ["quote", []], "pi", "e"]), math.e)

    def test_builtins(self):
        self.assertEqual(eval(["+", 2, 3], standard_env()), 5)
        self.assertEqual(eval(["-", 2, 3], standard_env()), -1)


def eval_str(*programs):
    return eval(read('(begin ' + ' '.join(programs) + ')'))


class LanguageTest(unittest.TestCase):
    def test_arithmetic(self):
        self.assertEqual(eval_str("(+ 2 3)"), 5)
        self.assertEqual(eval_str("(- 2 3)"), -1)
        self.assertEqual(eval_str("(* 2 3)"), 6)
        self.assertEqual(eval_str("(/ 4 2)"), 2)
        self.assertTrue(eval_str("(> 3 2)"))
        self.assertTrue(eval_str("(< 2 3)"))
        self.assertTrue(eval_str("(>= 3 2)"))
        self.assertTrue(eval_str("(>= 2 2)"))
        self.assertTrue(eval_str("(<= 2 3)"))
        self.assertTrue(eval_str("(<= 2 2)"))
        self.assertTrue(eval_str("(= 42 42)"))
        self.assertEqual(eval_str("(<< 4 1)"), 8)
        self.assertEqual(eval_str("(>> 4 2)"), 1)
        self.assertEqual(eval_str("(xor 5 10)"), 15)
        self.assertEqual(eval_str("(abs 6)"), 6)
        self.assertEqual(eval_str("(abs -6)"), 6)
        self.assertEqual(eval_str("(max 2 3)"), 3)
        self.assertEqual(eval_str("(min 2 3)"), 2)
        self.assertEqual(eval_str("(round 2.2)"), 2.0)
        self.assertEqual(eval_str("(round 2.5)"), 3.0)
        self.assertEqual(eval_str("(ceil 9.2)"), 10.0)
        self.assertEqual(eval_str("(factorial 6)"), 720)
        self.assertTrue(eval_str("(= (radians 180) pi)"))

    def test_equality(self):
        self.assertTrue(eval_str("(= 2 2)"))
        self.assertTrue(eval_str("(= 2.3 2.3)"))
        self.assertTrue(eval_str("(= 2 2.0)"))
        self.assertTrue(eval_str("(equal? (quote (1 2)) (quote (1 2)))"))
        self.assertTrue(eval_str("(eq? abs abs)"))

    def test_lists(self):
        self.assertEqual(eval_str("(cons 1 (quote (2 3)))"), [1, 2, 3])
        self.assertEqual(eval_str("(car (quote (2 3 4)))"), 2)
        self.assertEqual(eval_str("(cdr (quote (2 3 4)))"), [3, 4])
        self.assertEqual(
            eval_str("(append (quote (1 2)) (quote (9 10)))"), [1, 2, 9, 10])
        self.assertEqual(eval_str("(length (quote (1 2 3)))"), 3)
        self.assertEqual(eval_str("(list 1 2)"), [1, 2])
        self.assertEqual(eval_str("(list 1 2 3 4 5)"), [1, 2, 3, 4, 5])

    def test_type_predicates(self):
        self.assertTrue(eval_str("(number? 42)"))
        self.assertFalse(eval_str("(number? (quote (1 2)))"))
        self.assertTrue(eval_str("(list? (quote (1 2)))"))
        self.assertFalse(eval_str("(list? 42)"))
        self.assertFalse(eval_str("(null? (quote (1 2 3)))"))
        self.assertTrue(eval_str("(null? (quote ()))"))
        self.assertTrue(eval_str("(symbol? (quote x))"))
        self.assertFalse(eval_str("(symbol? 42)"))

    def test_conditionals(self):
        self.assertFalse(eval_str("(and 1 (> 2 3))"))
        self.assertTrue(eval_str("(and (< 2 3) (> 3 2))"))
        self.assertFalse(eval_str("(not 1)"))
        self.assertTrue(eval_str("(not (not 1))"))
        self.assertFalse(eval_str("(or (> 3 10) (> 1 10))"))
        self.assertTrue(eval_str("(or (> 3 2) (< 3 2))"))

    def test_print(self):
        import sys
        from StringIO import StringIO

        try:
            out = StringIO()
            sys.stdout = out
            # Side-effect
            self.assertEqual(eval_str("(print 42)"), None)
            # Confirm stdout has expected output
            self.assertEquals(out.getvalue().strip(), "42")
        finally:
            sys.stdout = sys.__stdout__

    def test_side_effects(self):
        self.assertEqual(eval_str("(begin (print 42) 99)"), 99)
        self.assertEqual(eval_str("(begin (print 42) (+ 2 3))"), 5)


class LispFunctionTest(unittest.TestCase):
    def test_procedure(self):
        params = List(["a", "b"])
        env = standard_env()
        body = List(["+", "a", "b"])
        proc = Procedure(params, body, env)
        self.assertEquals(proc(2, 3), 5)

    def test_function_definition(self):
        self.assertEqual(eval_str("((lambda (x) (+ x 42)) 2)"), 44)
        self.assertEqual(
            eval_str("""
        (begin
          (define times-ten
            (lambda (x)
              (* x 10)))
          (times-ten 50))
        """), 500)


class WriteReadTest(unittest.TestCase):
    def test_lisp_print_str(self):
        self.assertEqual(lisp_print_str(4), "4")
        self.assertEqual(lisp_print_str(List([2, 3])), "(2 3)")


class CodeLoadingTest(unittest.TestCase):
    def test_load_file(self):
        self.assertEqual(load_file("gol.scm"), Symbol("game-of-life-loaded"))

    def test_load_file_lisp(self):
        self.assertEqual(
            eval_str("(load-file (quote gol.scm))"),
            Symbol("game-of-life-loaded"))


# Adapted from http://rosettacode.org/wiki/Conway%27s_Game_of_Life#Scheme
class GameOfLife(unittest.TestCase):
    def test_blinker(self):
        # Game of Life impl in 2.2k, 82 lines
        load_file("gol.scm")
        self.assertEqual(eval_str('(nth 2 (quote (0 1 2 3)))'), 1)
        self.assertEqual(
            eval_str('(map (lambda (x) (* x 2)) (quote (2 4 6 8)))'),
            List([4, 8, 12, 16]))
        self.assertTrue(eval_str('''
        (= (next-universe (quote ((0 1 0)
                                  (0 1 0)
                                  (0 1 0))))
           (quote ((0 0 0)
                   (1 1 1)
                   (0 0 0))))'''))
        self.assertTrue(eval_str('''
        (= (next-universe (quote ((0 0 0)
                                  (1 1 1)
                                  (0 0 0))))
           (quote ((0 1 0)
                   (0 1 0)
                   (0 1 0))))'''))
        self.assertEqual(
            eval_str('''
        (conway (quote ((0 1 0)
                        (0 1 0)
                        (0 1 0)))
                5)'''), Symbol('done'))
