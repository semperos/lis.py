(define nth
  (lambda (n lst)
    (if (> n (length lst))
        0
        (if (< n 1)
            0
            (if (= n 1)
                (car lst)
                (nth (- n 1) (cdr lst)))))))

(define map
  (lambda (f coll)
    (if (null? coll)
        (quote ())
        (cons (f (car coll)) (map f (cdr coll))))))

(define next-universe
  (lambda (universe)
    (begin
      (define cell
        (lambda (x y)
          (if (list? (nth y universe))
              (nth x (nth y universe))
              0)))
      (define neighbor-sum
        (lambda (x y)
          (+ (cell (- x 1) (- y 1))
             (+ (cell (- x 1) y)
                (+ (cell (- x 1) (+ y 1))
                   (+ (cell x (- y 1))
                      (+ (cell x (+ y 1))
                         (+ (cell (+ x 1) (- y 1))
                            (+ (cell (+ x 1) y)
                               (cell (+ x 1) (+ y 1)))))))))))
      (define next-cell
        (lambda (x y)
          ((lambda (cur)
             ((lambda (ns)
                (if (and (= cur 1)
                         (or (< ns 2) (> ns 3)))
                    0
                    (if (and (= cur 0) (= ns 3))
                        1
                        cur))) (neighbor-sum x y))) (cell x y))))
      (define row
        (lambda (n out)
          ((lambda (w)
             (if (= (length out) w)
                 out
                 (row n
                      (cons (next-cell (- w (length out)) n)
                            out)))) (length (car universe)))))
      (define int-range
        (lambda (bot top)
          (if (> bot top)
              (quote ())
              (cons bot (int-range (+ bot 1) top)))))

      (map (lambda (n)
             (row n (quote ())))
           (int-range 1 (length universe))))))

(define display-universe
  (lambda (universe)
    (map (lambda (a-row)
           (begin
             (map (lambda (a-cell)
                    (print (if (= a-cell 1) (quote o) (quote .))))
                  a-row)
             (newline)))
         universe)))

(define conway
  (lambda (seed reps)
    (begin
      (newline)
      (if (> reps 0)
          (begin
            (display-universe seed)
            (newline)
            (conway (next-universe seed) (- reps 1)))
          (quote done)))))

(define run-blinker
  (lambda (reps)
    (conway (quote ((0 1 0)
                    (0 1 0)
                    (0 1 0)))
            reps)))

(define run-slider
  (lambda (reps)
    (conway (quote ((0 0 1 0 0 0 0 0)
                    (0 0 0 1 0 0 0 0)
                    (0 1 1 1 0 0 0 0)
                    (0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0)))
            reps)))

(define run-pentadecathlon
  (lambda (reps)
    (conway (quote ((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))
            reps)))

(define run-pulsar
  (lambda (reps)
    (conway (quote ((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 1 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 1 1 1 0 0 0 0 0 0)
                    (0 0 0 0 0 0 1 0 1 0 0 0 0 0 0)
                    (0 0 0 0 0 0 1 1 1 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 1 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))
            reps)))

(define run-repeat
  (lambda (reps)
    (conway (quote ((0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0)
                    (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0)
                    (1 1 1 0 0 0 1 1 1 0 0 0 0 1 0 0 0 0 0 1 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 0)
                    (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0)
                    (0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))
            reps)))

(define run-queen-bee-shuttle
  (lambda (reps)
    (conway (quote ((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))
            reps)))

(define run-rpento
  (lambda (reps)
    (conway (quote ((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))
            reps)))

(define run-toad
  (lambda (reps)
    (conway (quote ((0 0 0 0 0 0)
                    (0 0 0 0 0 0)
                    (0 0 1 1 1 0)
                    (0 1 1 1 0 0)
                    (0 0 0 0 0 0)
                    (0 0 0 0 0 0)))
            reps)))

(quote game-of-life-loaded)
